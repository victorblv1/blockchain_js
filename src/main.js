const {Blockchain, Transaction} = require('./blockchain');
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

const myKey = ec.keyFromPrivate("d742bd6a7ffeafebbe0e16992f4c6fe49a1a485069b4d49e2caac042eb32ab19");
const myWalletAddress = myKey.getPublic('hex');

let vikusCoin = new Blockchain();

const tx1 = new Transaction(myWalletAddress, 'public key goes here', 10);
tx1.signTransaction(myKey);
vikusCoin.addTransaction(tx1);

console.log("Starting the miner ...")
vikusCoin.minePendingTransactions("vikus-address");
// We need this line since the first return 0.
// vikusCoin.minePendingTransactions("vikus-address");

console.log("\nBalance of vikus is ", vikusCoin.getBalanceOfAddress("vikus-address"));
console.log("Is chain valid? ", vikusCoin.isChainValid(myWalletAddress));
